package com.assesment.hazesoft.database

import androidx.room.TypeConverter
import com.assesment.hazesoft.feature.shared.model.response.OriginalDto
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * @Created by: asthreshNabin
 * @Date: 17/08/2023
 */
class TypeConvertersOriginal {
    @TypeConverter // note this annotation
    fun stringToObject(optionValuesString: String?): OriginalDto? {
        if (optionValuesString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<OriginalDto>() {

        }.type
        return gson.fromJson<OriginalDto>(optionValuesString, type)
    }

    @TypeConverter
    fun objectToString(listValues: OriginalDto?): String? {
        if (listValues == null) {
            return null
        }

        val gson = Gson()
        val type = object : TypeToken<OriginalDto>() {

        }.type
        return gson.toJson(listValues, type)
    }
}