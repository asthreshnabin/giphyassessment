package com.assesment.hazesoft.database

import androidx.room.TypeConverter
import com.assesment.hazesoft.feature.shared.model.response.ImagesDto
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * @Created by: asthreshNabin
 * @Date: 17/08/2023
 */
class TypeConvertersImage {
    @TypeConverter // note this annotation
    fun stringToObject(optionValuesString: String?): ImagesDto? {
        if (optionValuesString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ImagesDto>() {

        }.type
        return gson.fromJson<ImagesDto>(optionValuesString, type)
    }

    @TypeConverter
    fun objectToString(listValues: ImagesDto?): String? {
        if (listValues == null) {
            return null
        }

        val gson = Gson()
        val type = object : TypeToken<ImagesDto>() {

        }.type
        return gson.toJson(listValues, type)
    }
}