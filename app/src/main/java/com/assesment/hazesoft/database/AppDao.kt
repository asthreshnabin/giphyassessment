package com.assesment.hazesoft.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.assesment.hazesoft.feature.shared.model.response.GiphyResponse
import com.assesment.hazesoft.utils.constants.DbConstants

/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
@Dao
interface AppDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFav(favItem: GiphyResponse?)

    @Query("SELECT * FROM ${DbConstants.tbGiphyResponse}")
    suspend fun getFavFromDatabase(): List<GiphyResponse>

    @Query("DELETE FROM ${DbConstants.tbGiphyResponse} WHERE id =:id")
    suspend fun deleteFavByEventId(
        id: String?
    )
}