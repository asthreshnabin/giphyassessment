package com.assesment.hazesoft.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.assesment.hazesoft.feature.shared.model.response.GiphyResponse
import com.assesment.hazesoft.utils.constants.DbConstants

/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
@Database(
    entities = [GiphyResponse::class],
    version = 1
)
@TypeConverters(
    TypeConvertersImage::class,
    TypeConvertersOriginal::class,
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getAppDao(): AppDao

    companion object {
        private var appDatabase: AppDatabase? = null

        fun getAppDatabase(context: Context?) =
            if (appDatabase != null && appDatabase?.isOpen == true) {
                appDatabase
            } else {
                if (context != null) {
                    Room.databaseBuilder(context, AppDatabase::class.java, DbConstants.dbName)
                        .fallbackToDestructiveMigration().build()
                } else {
                    null
                }
            }
    }
}