package com.assesment.hazesoft.feature.shared.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.assesment.hazesoft.databinding.AdapterGiphyBinding
import com.assesment.hazesoft.feature.shared.base.BaseAdapter
import com.assesment.hazesoft.feature.shared.model.response.GiphyResponse
import com.assesment.hazesoft.feature.shared.viewHolder.GiphyViewHolder

/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
class GiphyAdapter(
    private val context: Context,
    private val giphyResponse: List<GiphyResponse>?,
    private var isFav: Boolean? = false,
    private val onFavClicked: (favItem: GiphyResponse?) -> Unit,
    private val onUnFavClicked: (favItem: GiphyResponse?) -> Unit
) :
    BaseAdapter<GiphyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = GiphyViewHolder(
        AdapterGiphyBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        ), isFav,
        {
            onFavClicked(giphyResponse?.get(it ?: 0))
        },{
            onUnFavClicked(giphyResponse?.get(it ?: 0))
        })

    override fun getItemCount() = giphyResponse?.size ?: 0

    override fun onBindViewHolder(holder: GiphyViewHolder, position: Int) {
        holder.bind(giphyResponse?.get(position) ?: GiphyResponse(), context)
    }
}