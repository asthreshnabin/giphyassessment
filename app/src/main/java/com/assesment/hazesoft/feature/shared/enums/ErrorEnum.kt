package com.assesment.hazesoft.feature.shared.enums
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
enum class ErrorEnum{
    NoWifi,
    NotLoggedIn,
    ServerMaintenance,
    SessionExpired,
    DefaultError
}