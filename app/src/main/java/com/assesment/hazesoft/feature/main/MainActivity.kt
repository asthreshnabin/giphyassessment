package com.assesment.hazesoft.feature.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.viewModels
import androidx.viewpager.widget.ViewPager
import com.assesment.hazesoft.R
import com.assesment.hazesoft.databinding.ActivityMainBinding
import com.assesment.hazesoft.feature.main.fragment.FavouriteFragment
import com.assesment.hazesoft.feature.main.fragment.FavouriteViewModel
import com.assesment.hazesoft.feature.main.fragment.SearchFragment
import com.assesment.hazesoft.feature.shared.adapter.ViewPagerAdapter
import com.assesment.hazesoft.feature.shared.base.BaseActivity
import com.assesment.hazesoft.utils.util.Logger
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {
    private val mainViewModel: MainViewModel by viewModels()
    private val favouriteViewModel: FavouriteViewModel by viewModels()
    private var navLabels = arrayOf("Trending","Favourite")
    private var adapter: ViewPagerAdapter? = null

    override fun getViewModel() = mainViewModel

    override fun getViewBinding() = ActivityMainBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setup()
    }

    private fun setup() {
        initTabs()
        favouriteViewModel.isFav.observe(this) {
            if (it)
                FavouriteFragment().getFav()
        }
    }

    private fun initTabs() {
        setupViewPager(binding?.viewpager)
        binding?.tabs?.setupWithViewPager(binding?.viewpager)
        binding?.tabs?.tabGravity = TabLayout.GRAVITY_FILL
        binding?.viewpager?.measure(
            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
        )
        for (i in 0 until binding?.tabs?.tabCount!!) {
            val tab = LayoutInflater.from(this)
                .inflate(R.layout.tab_layout, null) as LinearLayout
            val tabLabel = tab.findViewById<View>(R.id.navLabelLeaderboard) as TextView
            tabLabel.text = navLabels[i]
            binding?.tabs?.getTabAt(i)?.customView = tab
        }
    }

    private fun setupViewPager(
        viewPager: ViewPager?
    ) {
        adapter = ViewPagerAdapter(supportFragmentManager)
        adapter?.addFrag(SearchFragment())
        adapter?.addFrag(FavouriteFragment())
        viewPager?.adapter = adapter
    }
}