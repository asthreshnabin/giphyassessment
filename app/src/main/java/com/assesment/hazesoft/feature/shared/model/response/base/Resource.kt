package com.assesment.hazesoft.feature.shared.model.response.base

import com.assesment.hazesoft.feature.shared.enums.Status
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
sealed class Resource<out T>(val status: Status, val data: T?, val throwable: Throwable?) {
    data class Success<T>(val response: T?) : Resource<T>(Status.SUCCESS, response, null)
    data class Error<T>(val error: Throwable?) : Resource<T>(Status.ERROR, null, error)
}