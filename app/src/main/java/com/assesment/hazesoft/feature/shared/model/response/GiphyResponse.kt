package com.assesment.hazesoft.feature.shared.model.response

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.assesment.hazesoft.utils.constants.DbConstants
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
@Parcelize
@Entity(tableName = DbConstants.tbGiphyResponse)
data class GiphyResponse(
    @PrimaryKey(autoGenerate = true) val roomId: Int? = null,
    @SerializedName("type")
    var type:String? = null,
    @SerializedName("id")
    var id:String? = null,
    @SerializedName("url")
    var url:String? = null,
    @SerializedName("images")
    var images:ImagesDto? = null,
): Parcelable

@Parcelize
data class ImagesDto(
    @SerializedName("original")
    var original:OriginalDto? = null,
):Parcelable

@Parcelize
data class OriginalDto(
    @SerializedName("url")
    var url:String? = null,
):Parcelable
