package com.assesment.hazesoft.feature.shared.module

import android.content.Context
import com.assesment.hazesoft.database.AppDatabase
import com.assesment.hazesoft.network.RetrofitHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    fun providesAppDatabase(@ApplicationContext context: Context) =
        AppDatabase.getAppDatabase(context)

    @Provides
    fun providesAppDao(appDatabase: AppDatabase?) = appDatabase?.getAppDao()

    @Provides
    fun providesApiService() = RetrofitHelper.getApiService()
}