package com.assesment.hazesoft.feature.shared.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.assesment.hazesoft.R
import com.assesment.hazesoft.feature.shared.enums.ErrorEnum
import com.assesment.hazesoft.utils.helper.ProgressDialogHelper
import com.assesment.hazesoft.utils.util.DialogUtils
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
abstract class BaseFragment<BD : ViewBinding, VM : BaseViewModel> : Fragment() {

    protected var binding: BD? = null
    private var viewModel: VM? = null
    private var progressDialogHelper: ProgressDialogHelper? = null

    abstract fun getViewModel(): VM
    abstract fun getViewBinding(): BD

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = getViewBinding()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = getViewModel()
        initObservers()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    protected fun showMessageDialog(message: String?) {
        DialogUtils.showAlertDialog(context, "", message, {}, {})
    }

    protected fun showMessageDialog(title: String?, message: String?) {
        DialogUtils.showAlertDialog(context, title, message, {}, {})
    }

    protected fun showMessageDialog(message: String?, okAction: (() -> Unit?)?) {
        DialogUtils.showAlertDialog(context, "", message, { okAction?.invoke() }, {})
    }

    protected fun showMessageDialog(title: String?, message: String?, okAction: () -> Unit?) {
        DialogUtils.showAlertDialog(context, title, message, { okAction() }, {})
    }

    protected fun showMessageDialog(title: Int?, message: Int?, okAction: (() -> Unit?)?) {
        DialogUtils.showAlertDialog(context,
            getString(title ?: 0),
            getString(message ?: 0),
            { okAction?.invoke() },
            {})
    }

    protected fun showLoading(message: String?) {
        if(progressDialogHelper==null)
            progressDialogHelper= ProgressDialogHelper(requireContext())
        progressDialogHelper?.showProgress(message)
    }

    protected fun hideLoading() {
        if(progressDialogHelper==null)
            progressDialogHelper= ProgressDialogHelper(requireContext())
        progressDialogHelper?.dismissDialog()
    }

    protected fun showMessageDialog(errorEnum: ErrorEnum?, okAction: (() -> Unit?)?) {
        when (errorEnum) {
            ErrorEnum.NoWifi -> {
                showMessageDialog(
                    R.string.error_no_wifi, R.string.error_no_wifi_description, okAction
                )
            }
            ErrorEnum.SessionExpired -> {
                showMessageDialog(
                    R.string.session_expired, R.string.session_expired_description, okAction
                )
            }
            ErrorEnum.DefaultError -> {
                showMessageDialog(R.string.error, R.string.error_default, okAction)
            }
            else -> {
                showMessageDialog(
                    R.string.error, R.string.error_default, okAction
                )
            }
        }
    }

    private fun showOrHideLoading(isShow: Boolean?, message: String?) {
        if (isShow == true) {
            showLoading(message)
        } else {
            hideLoading()
        }
    }

    private fun initObservers() {
        with(viewModel) {
            this?.errorResponse?.observe(viewLifecycleOwner) { dialogMessage ->
                showMessageDialog(dialogMessage.asString(context), okAction.value)
            }

            this?.errorEnumResponse?.observe(viewLifecycleOwner) { errorEnum ->
                showMessageDialog(errorEnum, okAction.value)
            }

            this?.isLoading?.observe(viewLifecycleOwner) { isLoading ->
                showOrHideLoading(isLoading, loadingMessage.value?.asString(context))
            }

        }
    }
}