package com.assesment.hazesoft.feature.shared.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}