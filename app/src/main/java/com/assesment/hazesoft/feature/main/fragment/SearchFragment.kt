package com.assesment.hazesoft.feature.main.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.assesment.hazesoft.databinding.FragmentSearchBinding
import com.assesment.hazesoft.feature.main.MainViewModel
import com.assesment.hazesoft.feature.shared.adapter.GiphyAdapter
import com.assesment.hazesoft.feature.shared.base.BaseFragment
import com.assesment.hazesoft.feature.shared.model.response.GiphyResponse
import com.assesment.hazesoft.utils.extensions.viewGone
import com.assesment.hazesoft.utils.extensions.viewVisible
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class SearchFragment : BaseFragment<FragmentSearchBinding, MainViewModel>() {
    private var keyword: String? = ""
    private val searchViewModel: MainViewModel by viewModels()
    private val favouriteViewModel: FavouriteViewModel by viewModels()
    private var giphyResponse: MutableList<GiphyResponse> = mutableListOf()
    private var searchresponse: MutableList<GiphyResponse> = mutableListOf()
    private var giphyAdapter: GiphyAdapter? = null
    private var searchAdapter: GiphyAdapter? = null

    private var indexTrending: Int = 1
    private var hasNextTrending: Boolean = true
    private var indexSearch: Int = 0
    private var hasNextSearch: Boolean = true

    override fun getViewModel() = searchViewModel

    override fun getViewBinding() = FragmentSearchBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    private fun setup() {
        giphyResponse.clear()
        initObservers()
        getTrending()
        initRecyclerView()
        initSearchTextListener()
    }

    private fun initSearchTextListener() {
        binding?.edtSearch?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                hideTrending(false)
            }

            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                if (text.toString().isNotEmpty() && text.toString().length > 2) {
                    keyword = text.toString()
                    hideTrending(true)
                    searchViewModel.doSearch(text.toString(), indexTrending)
                } else if (text.toString().isEmpty()) {
                    hideTrending(false)
                }
            }
        })
    }

    private fun hideTrending(show: Boolean) {
        if (show) {
            binding?.rcvGifs?.viewGone()
            binding?.rcvSearchItem?.viewVisible()
        } else {
            binding?.rcvGifs?.viewVisible()
            binding?.rcvSearchItem?.viewGone()
        }
    }

    private fun initObservers() {
        onTrendingResponse()
    }

    private fun getTrending() {
        searchViewModel.getTrending(indexTrending)
    }

    private fun onTrendingResponse() {
        searchViewModel.trendingResponse.observe(this) { trendingResponse ->
            initGiphyResponse(trendingResponse)
        }
        searchViewModel.searchResponse.observe(this) { trendingResponse ->
            initSearchGiphyResponse(trendingResponse)
        }
    }

    private fun initRecyclerView() {
        giphyAdapter =
            GiphyAdapter(
                requireContext(),
                giphyResponse as List<GiphyResponse>?,
                false,
                { favData ->
                    favouriteViewModel.saveFavToDatabase(favData)
                },
                {})
        val trendingLayout =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding?.rcvGifs?.layoutManager = trendingLayout
        binding?.rcvGifs?.adapter = giphyAdapter
        binding?.rcvGifs?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                    val totalItemCount = linearLayoutManager.itemCount
                    val lastVisible = linearLayoutManager.findLastCompletelyVisibleItemPosition()

                    val endHasBeenReached = (lastVisible + 1) >= totalItemCount
                    if (totalItemCount > 0 && endHasBeenReached) {
                        if (indexTrending > 1 && hasNextTrending) searchViewModel.getTrending(
                            indexTrending
                        )
                    }
                }
            }
        })
        searchAdapter =
            GiphyAdapter(
                requireContext(),
                searchresponse as List<GiphyResponse>?,
                false,
                { favData ->
                    favouriteViewModel.saveFavToDatabase(favData)
                },
                {})
        val searchLayout =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding?.rcvSearchItem?.layoutManager = searchLayout
        binding?.rcvSearchItem?.adapter = searchAdapter
        binding?.rcvSearchItem?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                    val totalItemCount = linearLayoutManager.itemCount
                    val lastVisible = linearLayoutManager.findLastCompletelyVisibleItemPosition()

                    val endHasBeenReached = (lastVisible + 1) >= totalItemCount
                    if (totalItemCount > 0 && endHasBeenReached) {
                        if (indexSearch > 1 && hasNextSearch) searchViewModel.doSearch(
                            keyword,
                            indexTrending
                        )
                    }
                }
            }
        })

    }

    private fun initGiphyResponse(list: List<GiphyResponse>?) {
        if (indexTrending <= 1) this.giphyResponse.clear()
        indexTrending += 1
        val startRange = giphyResponse.size
        this.giphyResponse.addAll(list as MutableList<GiphyResponse>)
        val endRange = giphyResponse.size
        Log.d("NS", giphyResponse.size.toString())

        if (list.isEmpty()) {
            hasNextTrending = false
        } else {
            giphyAdapter?.notifyItemRangeInserted(startRange, endRange)
        }
    }

    private fun initSearchGiphyResponse(list: List<GiphyResponse>?) {
        if (indexSearch <= 1) this.searchresponse.clear()
        this.searchresponse.addAll(list as MutableList<GiphyResponse>)
        if (list.isEmpty()) {
            hasNextSearch = false
        } else {
            searchAdapter?.notifyDataSetChanged()
        }
    }
}