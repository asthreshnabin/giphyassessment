package com.assesment.hazesoft.feature.shared.repository

import com.assesment.hazesoft.database.AppDao
import com.assesment.hazesoft.feature.shared.base.BaseRepository
import com.assesment.hazesoft.feature.shared.model.response.GiphyResponse
import com.assesment.hazesoft.feature.shared.model.response.base.Resource
import javax.inject.Inject

/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
class MainRepository @Inject constructor(private val appDao: AppDao?) : BaseRepository() {

    suspend fun getTrending(page: Int?): Resource<List<GiphyResponse>> {
        return try {
            apiService.getTrending(
                "ZVcriBfZUiJ7X7YjWtdMLPKR2MdErLDX", 25, page
            ).let { response ->
                if (response.isSuccessful && response.body() != null) {
                    Resource.Success(response.body()?.data ?: emptyList())
                } else {
                    Resource.Error(getError(response.code(), response.errorBody()?.string()))
                }
            }

        } catch (exception: Exception) {
            Resource.Error(getError(exception))
        }
    }

    suspend fun doSearch(query:String?,page: Int?): Resource<List<GiphyResponse>> {
        return try {
            apiService.doSearch(
                "ZVcriBfZUiJ7X7YjWtdMLPKR2MdErLDX", 25, page,query
            ).let { response ->
                if (response.isSuccessful && response.body() != null) {
                    Resource.Success(response.body()?.data ?: emptyList())
                } else {
                    Resource.Error(getError(response.code(), response.errorBody()?.string()))
                }
            }

        } catch (exception: Exception) {
            Resource.Error(getError(exception))
        }
    }

    suspend fun insertFav(favList:GiphyResponse?): Resource<Unit> {
        try {
            appDao?.insertFav(favList).let {
                return Resource.Success(it)
            }
        } catch (ex: Exception) {
            return Resource.Error(ex)
        }
    }

    suspend fun getFavFromDatabase(): Resource<List<GiphyResponse>> {
        try {
            appDao?.getFavFromDatabase().let {
                return Resource.Success(it)
            }
        } catch (ex: Exception) {
            return Resource.Error(ex)
        }
    }


    suspend fun deleteFavByEventId(favId : String?): Resource<Unit> {
        try {
            appDao?.deleteFavByEventId(favId).let {
                return Resource.Success(it)
            }
        } catch (e: Exception) {
            return Resource.Error(e)
        }
    }
}