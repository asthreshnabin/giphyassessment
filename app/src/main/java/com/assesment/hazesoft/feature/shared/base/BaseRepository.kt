package com.assesment.hazesoft.feature.shared.base

import com.assesment.hazesoft.feature.shared.model.response.error.ErrorResponse
import com.assesment.hazesoft.network.ApiService
import com.assesment.hazesoft.utils.constants.ErrorConstants
import com.assesment.hazesoft.utils.constants.HttpCodeConstants
import com.assesment.hazesoft.utils.exceptions.UnAuthorizedException
import com.assesment.hazesoft.utils.util.GlobalUtils
import com.google.gson.Gson
import okhttp3.RequestBody
import java.io.File
import java.io.IOException
import java.net.UnknownHostException
import javax.inject.Inject
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
abstract class BaseRepository {

    @Inject
    protected lateinit var apiService : ApiService

    fun getGlobalUtils(any: Any?): RequestBody = GlobalUtils.buildGson(any)

    fun getGlobalUtils(keyName: String?, fileName: String?, file: File?) =
        GlobalUtils.buildGson(keyName, fileName, file)

    fun getError(responseCode: Int?, error: String?): Throwable {
        return try {
            val gson = Gson()
            val root = gson.fromJson(error, ErrorResponse::class.java)
            val errorMessages = root?.errors

            if (!errorMessages.isNullOrEmpty()) {
                if (responseCode == HttpCodeConstants.unAuthorized) {
                    UnAuthorizedException(
                        errorMessages.getOrNull(0)?.detail
                            ?: ErrorConstants.defaultErrorMessage
                    )
                } else {
                    Throwable(errorMessages[0].detail)
                }

            } else {
                getDefaultError()
            }
        } catch (e: Exception) {
            getDefaultError()
        }
    }

    fun getError(throwable: Throwable) =
        if (throwable is UnknownHostException || throwable is IOException) {
            throwable
        } else {
            getDefaultError()
        }

    private fun getDefaultError() = Throwable(ErrorConstants.defaultErrorMessage)
}