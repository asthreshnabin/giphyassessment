package com.assesment.hazesoft.feature.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.assesment.hazesoft.R
import com.assesment.hazesoft.feature.shared.base.BaseViewModel
import com.assesment.hazesoft.feature.shared.enums.Status
import com.assesment.hazesoft.feature.shared.model.response.GiphyResponse
import com.assesment.hazesoft.feature.shared.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
@HiltViewModel
class MainViewModel @Inject constructor(
    private val mainRepository: MainRepository?,
) : BaseViewModel() {

    private val _trendingResponse = MutableLiveData<List<GiphyResponse>>()
    val trendingResponse: LiveData<List<GiphyResponse>> get() = _trendingResponse
    private val _searchResponse = MutableLiveData<List<GiphyResponse>>()
    val searchResponse: LiveData<List<GiphyResponse>> get() = _searchResponse

    fun getTrending(page:Int?) {
        try {
            viewModelScope.launch(Dispatchers.IO) {
                val deferredTrendingResponse = async { mainRepository?.getTrending(page) }
                val trendingResponse = deferredTrendingResponse.await()

                if (trendingResponse?.status == Status.SUCCESS) {
                    _trendingResponse.postValue(
                            trendingResponse.data?: emptyList()
                    )
                } else {
                    performActionOnException(
                        trendingResponse?.throwable
                    ) {}
                }
            }
        } catch (e: Exception) {
            performActionOnException(e) {}
        }
    }

    fun doSearch(query:String?,page:Int?) {
        try {
            viewModelScope.launch(Dispatchers.IO) {
                val deferredTrendingResponse = async { mainRepository?.doSearch(query,page) }
                val trendingResponse = deferredTrendingResponse.await()

                if (trendingResponse?.status == Status.SUCCESS) {
                    _searchResponse.postValue(
                            trendingResponse.data?: emptyList()
                    )
                } else {
                    performActionOnException(
                        trendingResponse?.throwable
                    ) {}
                }
            }
        } catch (e: Exception) {
            performActionOnException(e) {}
        }
    }
}