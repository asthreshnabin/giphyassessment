package com.assesment.hazesoft.feature.main.fragment

import com.assesment.hazesoft.feature.shared.base.BaseViewModel
import com.assesment.hazesoft.feature.shared.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
@HiltViewModel
class SearchViewModel @Inject constructor(
    private val mainRepository: MainRepository?,
) : BaseViewModel() {

}