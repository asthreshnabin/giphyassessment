package com.assesment.hazesoft.feature.shared.base

import androidx.recyclerview.widget.RecyclerView
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
abstract class BaseAdapter<V  : BaseViewHolder> : RecyclerView.Adapter<V>() {

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
    }
}