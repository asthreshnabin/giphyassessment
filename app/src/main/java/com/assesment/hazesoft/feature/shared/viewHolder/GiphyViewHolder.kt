package com.assesment.hazesoft.feature.shared.viewHolder

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.assesment.hazesoft.R
import com.assesment.hazesoft.databinding.AdapterGiphyBinding
import com.assesment.hazesoft.feature.shared.base.BaseViewHolder
import com.assesment.hazesoft.feature.shared.model.response.GiphyResponse
import com.assesment.hazesoft.utils.extensions.viewGone
import com.assesment.hazesoft.utils.extensions.viewVisible
import com.bumptech.glide.Glide


/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
class GiphyViewHolder(
    private val binding: AdapterGiphyBinding,
    private val isFav: Boolean?,
    private val onFavClicked: (position: Int?) -> Unit,
    private val onUnFavClicked: (position: Int?) -> Unit
) : BaseViewHolder(binding.root) {
    fun bind(giphyResponse: GiphyResponse, context: Context) {
        val drawable = CircularProgressDrawable(context)
        drawable.setColorSchemeColors(
            R.color.colorPrimary,
            R.color.colorSecondary,
            R.color.colorPrimary
        )
        drawable.centerRadius = 30f
        drawable.strokeWidth = 5f
        // set all other properties as you would see fit and start it
        // set all other properties as you would see fit and start it
        drawable.start()
        Glide.with(context)
            .load(giphyResponse.images?.original?.url)
            .placeholder(drawable)
            .centerCrop()
            .into(binding.imvimage)
        Glide.with(context)
            .load(giphyResponse.images?.original?.url)
            .placeholder(drawable)
            .centerCrop()
            .into(binding.imgGridImage)

        binding.rllFav.setOnClickListener {
            onFavClicked(adapterPosition)
            binding.imvFavIcon.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.heart1
                )
            )
        }

        binding.rllUnfav.setOnClickListener {
            onUnFavClicked(adapterPosition)
        }

        if (isFav == true) {
            binding.rllGrid.viewVisible()
            binding.rllColumn.viewGone()

        } else {
            binding.rllColumn.viewVisible()
            binding.rllGrid.viewGone()
        }
    }
}