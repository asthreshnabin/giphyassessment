package com.assesment.hazesoft.feature.main.fragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.assesment.hazesoft.feature.shared.base.BaseViewModel
import com.assesment.hazesoft.feature.shared.model.response.GiphyResponse
import com.assesment.hazesoft.feature.shared.repository.MainRepository
import com.assesment.hazesoft.utils.extensions.onFailure
import com.assesment.hazesoft.utils.extensions.onSuccess
import com.assesment.hazesoft.utils.helper.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @Created by: asthreshNabin
 * @Date: 17/08/2023
 */
@HiltViewModel
class FavouriteViewModel @Inject constructor(
    private val mainRepository: MainRepository?,
) : BaseViewModel() {
    private val _favResponse = MutableLiveData<List<GiphyResponse>>()
    val favResponse: LiveData<List<GiphyResponse>> get() = _favResponse

    private val _isFav = MutableLiveData<Boolean>()
    val isFav: LiveData<Boolean> get() = _isFav

    private val _onFavDeleted = SingleLiveEvent<Unit>()
    val onFavDeleted: LiveData<Unit> get() = _onFavDeleted

    fun saveFavToDatabase(favItem: GiphyResponse?) {
        viewModelScope.launch(Dispatchers.IO) {
            mainRepository?.insertFav(favItem)?.onSuccess {
                _isFav.postValue(true)
            }?.onFailure { throwable ->
                performActionOnException(throwable) {}
            }
        }
    }

    fun getFavListFromDatabase() {
        viewModelScope.launch(Dispatchers.IO) {
            mainRepository?.getFavFromDatabase()?.onSuccess { favResponse ->
                _favResponse.postValue(favResponse)
            }?.onFailure { throwable ->
                performActionOnException(throwable) {}
            }
        }
    }
    fun deleteFavByEventId(favId: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            mainRepository?.deleteFavByEventId(favId)
                ?.onSuccess {
                    _onFavDeleted.postValue(it)
                }?.onFailure { throwable ->
                    performActionOnException(throwable) {}
                }
        }
    }
}