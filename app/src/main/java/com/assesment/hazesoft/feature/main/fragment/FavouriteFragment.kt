package com.assesment.hazesoft.feature.main.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.assesment.hazesoft.databinding.FragmentFavouriteBinding
import com.assesment.hazesoft.feature.shared.adapter.GiphyAdapter
import com.assesment.hazesoft.feature.shared.base.BaseFragment
import com.assesment.hazesoft.feature.shared.model.response.GiphyResponse
import com.assesment.hazesoft.utils.extensions.viewGone
import com.assesment.hazesoft.utils.extensions.viewVisible
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FavouriteFragment : BaseFragment<FragmentFavouriteBinding, FavouriteViewModel>(),
    SwipeRefreshLayout.OnRefreshListener {

    private val favouriteViewModel: FavouriteViewModel by viewModels()
    private var giphyResponse: MutableList<GiphyResponse> = mutableListOf()
    private var giphyAdapter: GiphyAdapter? = null

    override fun getViewModel() = favouriteViewModel

    override fun getViewBinding() = FragmentFavouriteBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            setup()
        }
    }

    private fun setup() {
        getFav()
        binding?.srlRefresh?.isRefreshing = false
        binding?.srlRefresh?.setOnRefreshListener(this)
        giphyResponse.clear()
        initObservers()
        initRecyclerView()
    }

    private fun initObservers() {
        onTrendingResponse()
        favouriteViewModel.onFavDeleted.observe(
            viewLifecycleOwner
        ) {
            setup()
        }
    }

    fun getFav() {
        favouriteViewModel.getFavListFromDatabase()
    }

    private fun onTrendingResponse() {
        favouriteViewModel.favResponse.observe(viewLifecycleOwner) { trendingResponse ->
            initGiphyResponse(trendingResponse)
        }
        favouriteViewModel.isFav.observe(viewLifecycleOwner) {
            if (it)
                getFav()
        }
    }

    private fun initRecyclerView() {
        giphyAdapter =
            GiphyAdapter(requireContext(), giphyResponse as List<GiphyResponse>?, true, {}, {
                favouriteViewModel.deleteFavByEventId(it?.id)
            })
        val trendingLayout =
            GridLayoutManager(context, 2)
        binding?.rcvGifs?.layoutManager = trendingLayout
        binding?.rcvGifs?.adapter = giphyAdapter
        giphyAdapter?.notifyDataSetChanged()

    }

    private fun initGiphyResponse(list: List<GiphyResponse>?) {
        this.giphyResponse.clear()
        this.giphyResponse.addAll(list as MutableList<GiphyResponse>)
        if (this.giphyResponse.size >0&& this.giphyResponse.isNotEmpty()){
            binding?.rcvGifs?.viewVisible()
            binding?.txvEmpty?.viewGone()
        }else{
            binding?.txvEmpty?.viewVisible()
            binding?.rcvGifs?.viewGone()
        }
        giphyAdapter?.notifyDataSetChanged()
    }

    override fun onRefresh() {
        setup()
    }
}