package com.assesment.hazesoft.feature.shared.model.response.base

import com.google.gson.annotations.SerializedName
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
class BaseArrayResponse<T>(
    @SerializedName("data")
    val data: List<T?>? = null
)

