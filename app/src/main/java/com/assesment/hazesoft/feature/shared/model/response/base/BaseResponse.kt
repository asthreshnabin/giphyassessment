package com.assesment.hazesoft.feature.shared.model.response.base

import com.google.gson.annotations.SerializedName
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
data class BaseResponse<T>(
    @SerializedName("data")
    val data: T? = null
)