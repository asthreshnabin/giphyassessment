package com.assesment.hazesoft.feature.shared.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
@HiltAndroidApp
class HazeSoftApplication : Application() {
    private val tag = "HazeSoftBaseApplication"
}