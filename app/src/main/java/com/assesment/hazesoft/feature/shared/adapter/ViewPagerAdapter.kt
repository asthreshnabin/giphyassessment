package com.assesment.hazesoft.feature.shared.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import java.util.ArrayList

/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
class ViewPagerAdapter(manager: FragmentManager) :
    FragmentPagerAdapter(manager) {
    private val mFragmentList: MutableList<Fragment> = ArrayList()


    override fun getItem(position: Int): Fragment {
        return mFragmentList[position]
    }

    override fun getCount(): Int {
        return mFragmentList.size
    }

    fun addFrag(fragment: Fragment) {
        mFragmentList.add(fragment)
    }
}