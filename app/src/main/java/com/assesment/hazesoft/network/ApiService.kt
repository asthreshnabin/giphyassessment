package com.assesment.hazesoft.network

import com.assesment.hazesoft.feature.shared.model.response.GiphyResponse
import com.assesment.hazesoft.feature.shared.model.response.base.BaseResponse
import com.assesment.hazesoft.utils.constants.ApiConstants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
interface ApiService {

    @GET(ApiConstants.trending)
    suspend fun getTrending(
        @Query("api_key") apiKey: String? = null,
        @Query("limit") limit: Int? = null,
        @Query("offset") offset: Int? = null,
        @Query("rating") rating: String? = "g",
        @Query("bundle") bundle: String? = "messaging_non_clips",
    ): Response<BaseResponse<List<GiphyResponse>>>

    @GET(ApiConstants.search)
    suspend fun doSearch(
        @Query("api_key") apiKey: String? = null,
        @Query("limit") limit: Int? = null,
        @Query("offset") offset: Int? = null,
        @Query("q") query: String? = null,
        @Query("rating") rating: String? = "g",
        @Query("bundle") bundle: String? = "messaging_non_clips",
    ): Response<BaseResponse<List<GiphyResponse>>>
}
