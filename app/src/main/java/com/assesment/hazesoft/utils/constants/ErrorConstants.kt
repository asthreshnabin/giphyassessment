package com.assesment.hazesoft.utils.constants
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
object ErrorConstants {
    const val defaultErrorMessage = "Something went wrong!"
}