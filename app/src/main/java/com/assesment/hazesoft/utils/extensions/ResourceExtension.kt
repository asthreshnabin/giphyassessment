package com.assesment.hazesoft.utils.extensions

import com.assesment.hazesoft.feature.shared.model.response.base.Resource
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */

fun <T> Resource<T>.onSuccess(action: (T?) -> Unit): Resource<T> {
    if (this is Resource.Success) {
        action(response)
    }
    return this
}

fun <T> Resource<T>.onFailure(action: (Throwable?) -> Unit): Resource<T> {
    if (this is Resource.Error) {
        action(error)
    }
    return this
}