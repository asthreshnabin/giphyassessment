package com.assesment.hazesoft.utils.constants
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
object PreferenceConstants {
    const val preferenceName = "preference_assesment"
    const val accessToken = "access_token"
    const val refreshToken = "refresh_token"
    const val isLoggedIn= "isLoggedIn"
}