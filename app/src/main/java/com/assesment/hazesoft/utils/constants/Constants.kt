package com.assesment.hazesoft.utils.constants
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
object Constants {
    const val splashTime = 2000L //in milliseconds
    const val apiDelayTime = 2000L //in milliseconds
}