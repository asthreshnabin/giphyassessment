package com.assesment.hazesoft.utils.extensions

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */

fun <T> Flow<T>.throttleFirst(windowDuration: Long= 1000L): Flow<T> = flow {
    var lastEmissionTime = 0L
    collect { upstream ->
        val currentTime = System.currentTimeMillis()
        val mayEmit = currentTime - lastEmissionTime > windowDuration
        if (mayEmit)
        {
            lastEmissionTime = currentTime
            emit(upstream)
        }
    }
}