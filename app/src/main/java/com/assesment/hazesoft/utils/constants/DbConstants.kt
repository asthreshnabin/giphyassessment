package com.assesment.hazesoft.utils.constants

/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
object DbConstants {
    const val tbGiphyResponse = "tbGiphyResponse"
    const val dbName = "db_assesment"
}