package com.assesment.hazesoft.utils.extensions

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
fun Context.hideSoftKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Context.showToastMessage(message: String?, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}

fun Context.hasPermission(permissionType: String) =
    ContextCompat.checkSelfPermission(this, permissionType) == PackageManager.PERMISSION_GRANTED