package com.assesment.hazesoft.utils.exceptions
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
class UnAuthorizedException(message: String?) : Exception(message)