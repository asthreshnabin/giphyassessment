package com.assesment.hazesoft.utils.constants
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
object ApiConstants {
    const val baseUrl = "https://api.giphy.com/v1/gifs/"
    const val trending = "trending"
    const val search = "search"
}