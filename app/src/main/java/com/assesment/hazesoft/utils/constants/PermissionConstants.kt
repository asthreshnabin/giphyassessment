package com.assesment.hazesoft.utils.constants
/**
 * @Created by: asthreshNabin
 * @Date: 16/08/2023
 */
object PermissionConstants {
    const val requestCodeCameraAndStoragePermission = 101
}